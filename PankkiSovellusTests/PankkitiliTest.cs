﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PankkiSovellusTests
{
    [TestFixture]
    public class PankkitiliTest
    {

        /* PankkitiliSovellus sisältää:
        * Luokka, jonka nimi on Pankkitili.
        * Pankkitilissä on kolme toimintoa.
        * Tallettaa/Nostaa/Siirtää rahaa. Tarkistaa onko tilillä rahaa.
        */


        [Test]
        public void LuoPankkitili()
        {

            Pankkitili tili1 =  new Pankkitili(100);

            Assert.IsInstanceOf<Pankkitili>(tili1);

        }

        [Test]
        public void AsetaPankkitililleAlkusaldo()
        {
            Pankkitili tili1 = new Pankkitili(500);

            Assert.That(500, Is.EqualTo(tili1.Saldo));
        } 

    }
}
